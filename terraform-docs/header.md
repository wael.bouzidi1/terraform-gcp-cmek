## Terraform Project API
This Terraform module creates a kms Key ring and keys attached to this key ring. IAM bindings can be attached to the Keyring and will propagate to all the keys or you can attach bindings directly to an individaul key at creation.
The key ring and keys are suffixed by a set of random characters to avoid collisions. Important note destroying the ressources in terraform will not tremove from the project. 

Be sure you use the at least terraform 0.14 which is supported by this module. Use [tfenv](https://github.com/tfutils/tfenv) to switch to manage your Terraform version(s).

## Example Usage
```
module "project_api" {
  source = "../"

project_id = "etia-5321557420"

key_ring = {
    key_ring_name   = "testkeyring"
    key_ring_location = "global"
  }

crypto_keys = [
  {
    crypto_key_name   = "testkey1"
    version_template = {
      algorithm = "GOOGLE_SYMMETRIC_ENCRYPTION"
      protection_level = "SOFTWARE"

    }},
    {
    crypto_key_name   = "testkey2"
  }


]
}
```


