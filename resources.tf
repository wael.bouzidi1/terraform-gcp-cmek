resource "random_id" "primary" {
  byte_length = 5
}

resource "google_kms_key_ring" "key_ring" {
  name     = format("%s-%s", var.key_ring.key_ring_name, random_id.primary.b64_url) 
  project  = var.project_id
  location = var.key_ring.key_ring_location

}

/*
resource "google_kms_key_ring_iam_binding" "key_ring_iam_binding" {
  count =  contains(keys(var.key_ring), "role_to_bind") ? 1 : 0
  key_ring_id = google_kms_key_ring.key_ring.id
  role        = try(var.key_ring.role_to_bind.role, "test")
  members = try(var.key_ring.role_to_bind.members, ["test"])
  dynamic condition {
      for_each = try(var.key_ring.role_to_bind.condition.title, tobool("false")) != ""  ? [] : [1]
      content {
        title       = var.key_ring.role_to_bind.condition.title
        description = var.key_ring.role_to_bind.condition.description
        expression  = var.key_ring.role_to_bind.condition.expression
      }
  }
}
*/

resource "google_kms_crypto_key" "attached_crypto_keys" {
  for_each = {for key in var.crypto_keys:  key.crypto_key_name => key} 
  name     = format("%s-%s", each.value.crypto_key_name, random_id.primary.b64_url)  
  key_ring = google_kms_key_ring.key_ring.id
  purpose  = each.value.purpose

  dynamic version_template {
      for_each = try(each.value.version_template.algorithm, tobool("false")) != ""  ? [] : [1]
      content {
      algorithm = try(each.value.version_template.algorithm, null)
      protection_level = try(each.value.version_template.protection_level, null)
      }
  }

  lifecycle {
    prevent_destroy = true
  }
}



locals {
  key_roles = {for key in var.crypto_keys:  key.crypto_key_name => key if contains(keys(key), "role_to_bind")}
}


/*
resource "google_kms_crypto_key_iam_binding" "crypto_key" {
  for_each =  local.key_roles
  crypto_key_id = format("%s-%s", each.value.crypto_key_name, random_id.primary.b64_url)
  role        = each.value.role_to_bind.role
  members = ["testmember@bogus.fr"] 

}

*/