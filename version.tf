terraform {
  required_version = ">= 0.14"
  experiments = [module_variable_optional_attrs]


  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 3.89, < 5.0"
    }
    google-beta = {
      source  = "hashicorp/google-beta"
      version = ">= 3.89, < 5.0"
    }
    random = {
      source  = "hashicorp/random"
      version = ">= 2.3.1"
    }


  }
}