

output "crypto_keys" {
  description = "Key created by the module and attached to the top level keyring"
  value       = google_kms_crypto_key.attached_crypto_keys

}

output "crypto_keys" {
  description = "Key ring created  by the module"
  value       = google_kms_crypto_key.attached_crypto_keys

}