<!-- BEGIN_TF_DOCS -->
## Terraform Project API
This Terraform module creates a kms Key ring and keys attached to this key ring. IAM bindings can be attached to the Keyring and will propagate to all the keys or you can attach bindings directly to an individaul key at creation.
The key ring and keys are suffixed by a set of random characters to avoid collisions. Important note destroying the ressources in terraform will not tremove from the project.

Be sure you use the at least terraform 0.14 which is supported by this module. Use [tfenv](https://github.com/tfutils/tfenv) to switch to manage your Terraform version(s).

## Example Usage
```
module "project_api" {
  source = "../"

project_id = "etia-5321557420"

key_ring = {
    key_ring_name   = "testkeyring"
    key_ring_location = "global"
  }

crypto_keys = [
  {
    crypto_key_name   = "testkey1"
    version_template = {
      algorithm = "GOOGLE_SYMMETRIC_ENCRYPTION"
      protection_level = "SOFTWARE"

    }},
    {
    crypto_key_name   = "testkey2"
  }

]
}
```
## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | >= 3.89, < 5.0 |
| <a name="provider_random"></a> [random](#provider\_random) | >= 2.3.1 |
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.14 |
| <a name="requirement_google"></a> [google](#requirement\_google) | >= 3.89, < 5.0 |
| <a name="requirement_google-beta"></a> [google-beta](#requirement\_google-beta) | >= 3.89, < 5.0 |
| <a name="requirement_random"></a> [random](#requirement\_random) | >= 2.3.1 |
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_crypto_keys"></a> [crypto\_keys](#input\_crypto\_keys) | This is a list of object with each object representing a crypto key to be created in the key ring that is created in this module. The object contains the arguments needed to create the key. <br>    The keys are protected with a lifecycle hook to avoid their destruction (see warning at the top of this page https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/kms_crypto_key)<br>    The role\_to\_bind contains the role to be bound and the members which are the identities that will be granted the privilege of the role.<br>    It will be bound with google\_kms\_crypto\_key\_iam\_binding. | <pre>list(object({<br>    crypto_key_name   = string<br>    purpose = optional(string)<br>    rotation_period = optional(string)<br>    destroy_scheduled_duration = optional(string)<br>    version_template = optional(object({<br>      algorithm = string<br>      protection_level = string<br><br>    }))<br>    role_to_bind = optional(object({<br>      role = string<br>      members = list(string)<br>      condition = optional(object({<br>        title       = string<br>        description = string<br>        expression  = string<br><br>        }))<br>    }))<br>  })<br>  )</pre> | n/a | yes |
| <a name="input_key_ring"></a> [key\_ring](#input\_key\_ring) | This object contains the parameters to create the top level key ring. The role\_to\_bind contains the role to be bound and the members which are the identities that will be granted the privilege of the role.<br>    The role will be bound with google\_kms\_key\_ring\_iam\_binding which is authoritative for a given role. Updates the IAM policy to grant a role to a list of members. <br>    Other roles within the IAM policy for the key ring are preserved. | <pre>object({<br>    key_ring_name   = string<br>    key_ring_location = string<br>    role_to_bind = optional(object({<br>      role = string<br>      members = list(string)<br>      condition = optional(object({<br>        title       = string<br>        description = string<br>        expression  = string<br>        }))<br><br>    }))<br>  })</pre> | n/a | yes |
| <a name="input_project_id"></a> [project\_id](#input\_project\_id) | Project id where the keyring will be created. | `string` | n/a | yes |
## Outputs

| Name | Description |
|------|-------------|
| <a name="output_crypto_keys"></a> [crypto\_keys](#output\_crypto\_keys) | Key ring created  by the module |
## Resources

| Name | Type |
|------|------|
| [google_kms_crypto_key.attached_crypto_keys](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/kms_crypto_key) | resource |
| [google_kms_key_ring.key_ring](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/kms_key_ring) | resource |
| [random_id.primary](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/id) | resource |

## Copyright (c)
> ###### Copyright (C) Wabion AG - Part of Accenture - All Rights Reserved
> ###### Unauthorized copying of this file, via any medium is strictly prohibited 
> ###### Proprietary and confidential
<!-- END_TF_DOCS -->
