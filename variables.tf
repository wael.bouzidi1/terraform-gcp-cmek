variable "project_id" {
  description = "Project id where the keyring will be created."
  type        = string
}

variable "key_ring" {
  description = <<EOF
    This object contains the parameters to create the top level key ring. The role_to_bind contains the role to be bound and the members which are the identities that will be granted the privilege of the role.
    The role will be bound with google_kms_key_ring_iam_binding which is authoritative for a given role. Updates the IAM policy to grant a role to a list of members. 
    Other roles within the IAM policy for the key ring are preserved.
  EOF
  type = object({
    key_ring_name   = string
    key_ring_location = string
    role_to_bind = optional(object({
      role = string
      members = list(string)
      condition = optional(object({
        title       = string
        description = string
        expression  = string
        }))

    }))
  })
}

variable "crypto_keys" {
  description = <<EOF
    This is a list of object with each object representing a crypto key to be created in the key ring that is created in this module. The object contains the arguments needed to create the key. 
    The keys are protected with a lifecycle hook to avoid their destruction (see warning at the top of this page https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/kms_crypto_key)
    The role_to_bind contains the role to be bound and the members which are the identities that will be granted the privilege of the role.
    It will be bound with google_kms_crypto_key_iam_binding.

  EOF
  type = list(object({
    crypto_key_name   = string
    purpose = optional(string)
    rotation_period = optional(string)
    destroy_scheduled_duration = optional(string)
    version_template = optional(object({
      algorithm = string
      protection_level = string

    }))
    role_to_bind = optional(object({
      role = string
      members = list(string)
      condition = optional(object({
        title       = string
        description = string
        expression  = string

        }))
    }))
  })
  )
  }

